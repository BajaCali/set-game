//
//  SetGameViewModel.swift
//  Set Game
//
//  Created by Michal Němec on 09.07.2021.
//

import Foundation

class SetGameViewModel: ObservableObject {
    typealias Card = SetGame.Card
    @Published private var model: SetGame = SetGame(numberOfPlayers: 2)
    var cardsOnTable: [Card] {
        model.cardsOnTable
    }
    
    var cardsInDeck: [Card] {
        model.deck
    }
    
    var isCorrectSet: Bool? {
        model.isCorrectSet
    }
    
    var isDeckEmpty: Bool {
        model.deck.isEmpty
    }
    
    var playerOne: Player {
        model.players[0]
    }
    
    var playerTwo: Player {
        model.players[1]
    }
    
    // MARK: - Intents
    
    func SETCalled(by player: Player) {
        model.SETCalled(by: player)
    }
    
    func newGame() {
        model = SetGame(numberOfPlayers: 2)
    }
    
    func startGame() {
        model.startGame()
    }
    
    func choose(_ card: Card) {
        model.choose(card)
    }
    
    func addThreeMoreCards() {
        model.dealThreeMoreCards()
    }
}

//
//  ArrayGetFirst.swift
//  Set Game
//
//  Created by Michal Němec on 12.07.2021.
//

import Foundation

extension Array {
    mutating func getFirst(_ k: Int) -> [Element] {
        assert(k > 0)
        assert(self.count >= k)
        
        var returnArray: [Element] = []
        
        for _ in 0..<k {
            returnArray.append(self.removeLast())
        }
        
        return returnArray
    }
}

//
//  Player.swift
//  Set Game
//
//  Created by Michal Němec on 13.07.2021.
//

import Foundation

struct Player: Identifiable {
    var score: Int
    var choosingSet: Bool
    var collectedCards: [SetGame.Card]
    let id: Int
    let name: String
    
    init(id: Int, name: String) {
        score = 0
        choosingSet = false
        self.id = id
        self.name = name
        collectedCards = []
    }
}

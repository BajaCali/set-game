//
//  SetGame.swift
//  Set Game
//
//  Created by Michal Němec on 08.07.2021.
//

import Foundation

struct SetGame  {
    private(set) var deck: [Card]
    private(set) var cardsOnTable: [Card]
    private var selectedCards: [Card] {
        cardsOnTable.filter { $0.isSelected }
    }
    var isCorrectSet: Bool? {
        if selectedCards.count != 3 { return nil }
        return isSet(cards: selectedCards)
    }
    var players: [Player]
    
    init(numberOfPlayers: Int) {
        deck = SetGame.createDeck().shuffled()
        cardsOnTable = []
        players = []
        for index in 0..<numberOfPlayers {
            players.append(Player(id: index, name: "Player \(index + 1)"))
        }
    }
    
    mutating func startGame() {
        cardsOnTable.append(contentsOf: deck.getFirst(12))
    }
    
    mutating func choose(_ card: Card) {
        if selectedCards.count < 3 { selectOrDeselect(card: card) }
        if let correctSet = isCorrectSet {
            if correctSet {
                players[0].score += 2
                moveSelectedCardsTo(playerOnIndex: 0)
            }
            for index in players.indices { players[index].choosingSet = false }
            for index in cardsOnTable.indices { cardsOnTable[index].isSelected = false }
        }
    }
    
    mutating private func selectOrDeselect(card: Card) {
        if let cardIndex = cardsOnTable.firstIndex(where: { $0.id == card.id }) {
            cardsOnTable[cardIndex].isSelected.toggle()
        }
    }
    
    private mutating func moveSelectedCardsTo(playerOnIndex playerIndex: Int) {
        for _ in 0..<selectedCards.count {
            if let cardIndex = cardsOnTable.firstIndex(where: { $0.isSelected }) {
                cardsOnTable[cardIndex].isSelected = false
                players[playerIndex].collectedCards.append(
                    deck.isEmpty || cardsOnTable.count > 12 ? cardsOnTable.remove(at: cardIndex)
                    : cardsOnTable.replace(at: cardIndex, with: deck.removeLast())
                )
            }
        }
    }
    
    mutating func SETCalled(by player: Player) {
        if let playerIndex = players.firstIndex(where: {$0.id == player.id}),
           players.firstIndex(where: { $0.choosingSet }) == nil {
            players[playerIndex].choosingSet.toggle()
        }
    }
   
    mutating func dealThreeMoreCards() {
        if deck.isEmpty { return }
        if (isSetOnTable()) { players[0].score -= 1 }
        cardsOnTable.append(contentsOf: deck.getFirst(3))
    }
    
    private func isSet(cards: [Card]) -> Bool {
        var allFeatures: [[TriState]] = [[], [], [], []]
        cards.forEach { card in
            for i in 0..<4 { allFeatures[i].append(card.features[i]) }
        }
        
        return allFeatures.reduce(true) { prevResult, feature in
            prevResult && (TriState.allCommon(states: feature) || TriState.allDiffer(states: feature))
        }
    }
    
    private func isSetOnTable() -> Bool {
        let cards = cardsOnTable.count
        for firstIndex in 0..<(cards - 2) {
            for secondIndex in (firstIndex + 1)..<(cards - 1) {
                for thirdIndex in (secondIndex + 1)..<cards {
                    if isSet(cards: [cardsOnTable[firstIndex], cardsOnTable[secondIndex], cardsOnTable[thirdIndex]]) {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    private static func createDeck() -> [Card] {
        var cards: [Card] = []
        
        // TODO: change back to 81 cards, uncoment and delete hardcoded simplifying features
        for index in 0..<81 {
            cards.append(Card(
                features: [
                    TriState.allCases[index / 27],
                    TriState.allCases[(index / 9) % 3],
                    TriState.allCases[(index / 3) % 3],
                    TriState.allCases[index % 3]
                ],
                id: index)
            )
        }
        return cards
    }
}

extension SetGame {
    struct Card: Identifiable, Hashable {
        fileprivate let features: [TriState]
        let id: Int
        var isSelected = false
        
        var shape: TriState { features[0] }
        var color: TriState { features[1] }
        var number: TriState { features[2] }
        var shading: TriState { features[3] }
    }
}

extension Array {
    mutating func replace(at index: Int, with element: Element) -> Element {
        let returnElement = self[index]
        self[index] = element
        return returnElement
    }
}

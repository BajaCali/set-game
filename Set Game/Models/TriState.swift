//
//  TriState.swift
//  Set Game
//
//  Created by Michal Němec on 09.07.2021.
//

import Foundation

enum TriState: Int, Equatable, CaseIterable { case first = 0, second, third }

extension TriState {
    static func allCommon(states: [TriState]) -> Bool {
        states.allSatisfy { $0 == states.first! }
    }
    
    static func allDiffer(states: [TriState]) -> Bool {
        TriState.allCases.allSatisfy {
            states.firstIndex(of: $0) == states.lastIndex(of: $0)
        }
    }
}

//
//  CardView.swift
//  Set Game
//
//  Created by Michal Němec on 09.07.2021.
//

import SwiftUI


struct CardView: View {
    let card: SetGame.Card
    let isCorrectSet: Bool?
    let isFaceUp: Bool
    
    let colors: [Color] = [.red, .purple, .green]
    
    var body: some View {
        let color = colors[card.color.rawValue]
        ZStack {
            GeometryReader { geometry in
                HStack {
                    let symbolShape = shape(card.shape)
                    ForEach(0..<card.number.rawValue + 1, id: \.self) { _ in
                        ZStack {
                            symbolShape
                                .stroke(lineWidth: DrawingConstants.symbolBorderWidth)
                                .fill(color)
                                .aspectRatio(1/2, contentMode: .fit)
                            symbolShape
                                .fill(color)
                                .aspectRatio(1/2, contentMode: .fit)
                                .opacity(Double(card.shading.rawValue) / 2.0)
                        }
                    }
                }
                .padding(geometry.size.height * DrawingConstants.percentInnerCardPadding)
                .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
            }
        }
        .cardify(isFaceUp: isFaceUp, isSelected: card.isSelected, color: color).animation(.easeInOut)
    }
    
    private func shape(_ number: TriState) -> some Shape {
        switch number {
        case .first: return AnyShape(Rectangle())
        case .second: return AnyShape(Diamond())
        case .third: return AnyShape(RoundedRectangle(cornerRadius: .infinity))
        }
    }
    
    private struct DrawingConstants {
        static let percentInnerCardPadding: CGFloat = 0.175
        static let cardCorderRadius: CGFloat = 5
       static let symbolBorderWidth: CGFloat = 2
    }
}

// taken from StackOverflow #61503390
fileprivate struct AnyShape: Shape {
    init<S: Shape>(_ wrapped: S) {
        _path = { rect in
            let path = wrapped.path(in: rect)
            return path
        }
    }
    
    func path(in rect: CGRect) -> Path {
        return _path(rect)
    }
    
    private let _path: (CGRect) -> Path
}

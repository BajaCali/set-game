//
//  Cardify.swift
//  Memorize
//
//  Created by Michal Němec on 14.07.2021.
//

import SwiftUI

struct Cardify: AnimatableModifier {
    let color: Color
    @Environment(\.colorScheme) var colorScheme

    var animatableData: Double {
        get { rotation }
        set { rotation = newValue }
    }

    var rotation: Double // in degrees
    var borderWidth: CGFloat

    private var isFaceUp: Bool {
        rotation < 60
    }

    init(isFaceUp: Bool, isSelected: Bool, color: Color){
        self.color = color
        self.rotation = isFaceUp ? 0 : 180
        self.borderWidth = isSelected ? DrawingConstants.selectedCardBorderWidth : DrawingConstants.cardBorderWidth
    }

    func body(content: Content) -> some View {
        return GeometryReader(content: {geometry in
            ZStack {
                let shape = RoundedRectangle(cornerRadius: DrawingConstants.cornerRadius)
                let cardBackgroundColor: Color = colorScheme == .dark ? Color.black : Color.white
                shape.fill(cardBackgroundColor)
                if isFaceUp {
                    shape
                        .strokeBorder(color, style: StrokeStyle(lineWidth: borderWidth))
                    content
                } else {
                    cardBackBody
                }
            }
            .rotation3DEffect(Angle.degrees(rotation), axis: (1, 0, 0))
        })
    }
    
    private var cardBackBody: some View {
        return GeometryReader { geometry in
            ZStack(alignment: .center) {
                RoundedRectangle(cornerRadius: DrawingConstants.cornerRadius).fill(colorScheme == .dark ? Color.white : Color.black)
                Diamond()
                    .fill(LinearGradient(gradient: Gradient(colors: [Color.red, Color.blue]), startPoint: .topLeading, endPoint: .bottomTrailing))
                    .aspectRatio(1/2, contentMode: .fit)
                    .padding(geometry.size.height * DrawingConstants.percentInnerCardPadding)
            }
            .frame(width: geometry.size.width, height: geometry.size.height)
        }
   }
    
    private struct DrawingConstants {
        static let cornerRadius: CGFloat = 10
        static let cardBorderWidth: CGFloat = 3
        static let selectedCardBorderWidth: CGFloat = 6
        static let percentInnerCardPadding: CGFloat = 0.175
    }
}


extension View {
    
    func cardify(isFaceUp: Bool, isSelected: Bool, color: Color) -> some View {
        self.modifier(Cardify(isFaceUp: isFaceUp, isSelected: isSelected, color: color))
    }
}

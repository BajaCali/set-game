//
//  SetGameView.swift
//  Set Game
//
//  Created by Michal Němec on 08.07.2021.
//

import SwiftUI

struct SetGameView: View {
    
    @ObservedObject var game = SetGameViewModel()
    
    @Namespace private var tableNamespace;
    
    init() {
        deckRandomization = [(Double, CGFloat, CGFloat)]()
        for _ in 0..<81 {
            let angle = Double.random(in: -DrawingConstants.maxRotationAngle..<DrawingConstants.maxRotationAngle)
            
            let xyFrom = 0.5 - DrawingConstants.maxCenterOffset
            let xyTo = 0.5 + DrawingConstants.maxCenterOffset
            let x = CGFloat.random(in: xyFrom..<xyTo)
            let y = CGFloat.random(in: xyFrom..<xyTo)
            deckRandomization.append((angle, x, y))
        }
    }
    
    var body: some View {
        VStack {
            gameControls
            gameBody
            HStack {
                discardPile
                Spacer()
            }
            .frame(height: DrawingConstants.discardPileHeight)
       }
        .padding()
    }
    
    private var gameControls: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading) {
            Text("SET Game")
                .font(.largeTitle)
                .onTapGesture {
                    withAnimation {
                        faceUpCards.removeAll()
                        game.newGame()
                    }
                }
                HStack(spacing: 0) {
                    Text("Score: ")
                    Text("\(game.playerOne.score)").bold()
                }
            }
            Spacer()
            deckBody
        }
    }
    
    private var deckRandomization: [(angle: Double, x: CGFloat, y: CGFloat)]
    private var deckBody: some View {
        ZStack {
            Color.clear
            ForEach(game.cardsInDeck) { card in
                GeometryReader { geometry in
                    let cardRandomization = deckRandomization[game.cardsInDeck.firstIndex(of: card) ?? 0]
                    CardView(card: card, isCorrectSet: nil, isFaceUp: false)
                        .zIndex(zIndex(of: card))
                        .matchedGeometryEffect(id: card.id, in: tableNamespace)
                        .rotationEffect(Angle.degrees(cardRandomization.angle * Double(DrawingConstants.deckAntiRandomization)),
                                        anchor: .init(x: cardRandomization.x * DrawingConstants.deckAntiRandomization,
                                                      y: cardRandomization.y * DrawingConstants.deckAntiRandomization
                                                     ))
                }
            }
        }
        .aspectRatio(DrawingConstants.cardAspectRatio, contentMode: .fit)
        .frame(height: DrawingConstants.undealtCardHeight)
        .onTapGesture {
            withAnimation {
                if faceUpCards.isEmpty {
                    game.startGame()
                } else {
                    game.addThreeMoreCards()
                }
            }
        }
    }
    
    private func zIndex(of card: SetGame.Card) -> Double {
        -Double(game.cardsInDeck.firstIndex(where: { $0.id == card.id }) ?? 0 )
    }
    
    @State private var faceUpCards = Set<Int>()
    
    private var gameBody: some View {
        CenteredAspectVGrid(items: game.cardsOnTable, aspectRatio: DrawingConstants.cardAspectRatio) { card in
            CardView(card: card, isCorrectSet: game.isCorrectSet, isFaceUp: faceUpCards.contains(where: { $0 == card.id }))
                .padding(DrawingConstants.paddingAroundCardsOnTable)
                .matchedGeometryEffect(id: card.id, in: tableNamespace)
                .onTapGesture {
                    withAnimation {
                        game.choose(card)
                    }
                }
                .onAppear {
                    faceUpCards.insert(card.id)
                }
        }
    }
    
    @ViewBuilder
    private var discardPile: some View {
        ZStack {
            if game.playerOne.collectedCards.isEmpty {
                Rectangle()
                    .opacity(0)
            } else {
                ForEach(game.playerOne.collectedCards) { card in
                    let cardRandoms = deckRandomization[card.id % 81]
                    CardView(card: card, isCorrectSet: false, isFaceUp: true)
                        .matchedGeometryEffect(id: card.id, in: tableNamespace)
                        .rotationEffect(Angle.degrees(cardRandoms.angle), anchor: .init(x: cardRandoms.x, y: cardRandoms.y))

                }
            }
        }
        .aspectRatio(DrawingConstants.cardAspectRatio, contentMode: .fit)
    }

    private struct DrawingConstants {
        static let cardAspectRatio: CGFloat = 3/2
        static let undealtCardHeight: CGFloat = 55
        static let paddingAroundCardsOnTable: CGFloat = 4
        
        static let maxRotationAngle: Double = 12
        static let maxCenterOffset: CGFloat = 0.8
        static let deckAntiRandomization: CGFloat = 0.4
        
        static let discardPileHeight: CGFloat = 65
        static let selectAnimationDuration: Double = 0.25
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
//        SetGameView()
        SetGameView()
            .preferredColorScheme(.dark)
    }
}

//
//  Diamond.swift
//  Set Game
//
//  Created by Michal Němec on 11.07.2021.
//

import SwiftUI

struct Diamond: Shape {
    
    func path(in rect: CGRect) -> Path {
        var p = Path()
        
        p.addLines([
            CGPoint(x: rect.midX, y: 0),
            CGPoint(x: rect.maxX, y: rect.midY),
            CGPoint(x: rect.midX, y: rect.maxY),
            CGPoint(x: 0, y: rect.midY),
            CGPoint(x: rect.midX, y: 0),
            CGPoint(x: rect.maxX, y: rect.midY)
        ])

        return p
    }
    
}

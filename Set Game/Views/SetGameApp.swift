//
//  SetGameApp.swift
//  Set Game
//
//  Created by Michal Němec on 08.07.2021.
//

import SwiftUI

@main
struct SetGameApp: App {
    var body: some Scene {
        WindowGroup {
            SetGameView()
        }
    }
}

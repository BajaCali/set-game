//
//  TriStateTests.swift
//  SetGameTests
//
//  Created by Michal Němec on 09.07.2021.
//

import XCTest

@testable
import Set_Game

class TriStateTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testAllCommon() throws {
        XCTAssert(TriState.allCommon(states: [TriState.second, TriState.second, TriState.second]))
    }
    
    func tesAllCommonFalse() throws {
        XCTAssert(!TriState.allCommon(states: [TriState.first, TriState.second, TriState.second]))
    }
    
    func testAllCommonEmptyArray() throws {
        XCTAssert(TriState.allCommon(states: []))
    }

    func testAllDiffer() throws {
        XCTAssert(TriState.allDiffer(states: [TriState.first, TriState.second, TriState.third]))
    }
    
    func testAllDifferFalse() throws {
        XCTAssert(!TriState.allDiffer(states: [TriState.first, TriState.second, TriState.first]))
    }
    
    func testAllDifferTwoStatesOnly() throws {
        XCTAssert(TriState.allDiffer(states: [.first, .second]))
    }
}
